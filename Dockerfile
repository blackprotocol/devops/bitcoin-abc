FROM alpine:3.12.1

ENV BITCOIN_ABC_VERSION=0.22.8
ENV BITCOIN_ABC_ASC=jasonbcox-sha256sums.${BITCOIN_ABC_VERSION}.asc
ENV BITCOIN_ABC_TAR=bitcoin-abc-${BITCOIN_ABC_VERSION}-x86_64-linux-gnu.tar.gz
ENV BITCOIN_ABC_PGP_KEY=3BB16D00D9A6D281591BDC76E4486356E7A81D2C
ENV GLIBC_VERSION=2.28-r0
ENV BITCOIN_DATA=/home/bitcoin/.bitcoin

WORKDIR /opt/bitcoin

RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
	&& wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk \
	&& wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-bin-${GLIBC_VERSION}.apk

RUN apk update \
	&& apk --no-cache add ca-certificates gnupg bash su-exec \
	&& apk --no-cache add glibc-${GLIBC_VERSION}.apk \
	&& apk --no-cache add glibc-bin-${GLIBC_VERSION}.apk

RUN wget https://download.bitcoinabc.org/${BITCOIN_ABC_VERSION}/linux/${BITCOIN_ABC_TAR} \
	&& wget https://download.bitcoinabc.org/${BITCOIN_ABC_VERSION}/${BITCOIN_ABC_ASC}

RUN gpg --keyserver keyserver.ubuntu.com --recv-keys ${BITCOIN_ABC_PGP_KEY} \
	&& gpg --verify ${BITCOIN_ABC_ASC} \
	&& grep ${BITCOIN_ABC_TAR} ${BITCOIN_ABC_ASC} | sha256sum -c \
	&& mkdir bitcoin \
	&& tar xzvf ${BITCOIN_ABC_TAR} --strip-components=1 -C bitcoin \
	&& mkdir /root/.bitcoin \
	&& mv bitcoin/bin/* /usr/local/bin/ \
	&& mv bitcoin/lib/* /usr/local/lib/ \
	&& apk del wget ca-certificates \
	&& rm -rf bitcoin* \
	&& rm -rf glibc-*

RUN adduser -S bitcoin
COPY ./scripts /scripts

EXPOSE 8332 8333 18332 18333 18443 18444
VOLUME ["/home/bitcoin/.bitcoin"]

ENTRYPOINT ["/scripts/entrypoint.sh"]
CMD ["bitcoind"]
